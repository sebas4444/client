package ar.com.seba.bank.client;

import ar.com.seba.bank.client.entities.Client;
import ar.com.seba.bank.client.entities.User;
import ar.com.seba.bank.client.repositories.AltamiraRepository;
import ar.com.seba.bank.client.repositories.ClientRepository;
import ar.com.seba.bank.client.repositories.UsersRepository;
import ar.com.seba.bank.client.services.ClientService;
import ar.com.seba.bank.client.services.ClientServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@SpringBootTest
class ClientServiceTest {

    @MockBean
    AltamiraRepository altamiraRepository;

    @MockBean
    ClientRepository clientRepository;

    @MockBean
    UsersRepository usersRepository;

    @Autowired
    ClientService service;

    @BeforeEach
    void setUp() {
        altamiraRepository = mock(AltamiraRepository.class);
        clientRepository = mock(ClientRepository.class);
        usersRepository = mock(UsersRepository.class);
        service = new ClientServiceImpl(clientRepository, altamiraRepository, usersRepository);
    }

    @Test
    void findByDocNum() {
        when(altamiraRepository.getDatosCliente(anyInt())).thenReturn(getClientA());
        Client client = service.findByDocNum(12203847);
        assertEquals(client.getDocNum(), 12203847);
    }

    @Test
    void getNewClientsPotential() {
        when(usersRepository.findAll()).thenReturn(getClientsPotencials());
        List newClientsPotencial = service.getNewClientsPotential();
        assertEquals(newClientsPotencial.size(), 3);
    }

    private User getUserA() {
        User client = new User();
        client.setDocNum("12203847");
        client.setId("12312L");
        return client;
    }

    private User getUserB() {
        User client = new User();
        client.setDocNum("12203848");
        client.setId("12313L");
        return client;
    }

    private User getUserC() {
        User client = new User();
        client.setDocNum("12203849");
        client.setId("12314L");
        return client;
    }


    private Client getClientA() {
        Client client = new Client();
        client.setBirthday(new Date());
        client.setDocNum(12203847);
        client.setId(12312L);
        client.setLastName("Landa");
        client.setName("Lalo");
        client.setTipDoc(4);
        return client;
    }

    private List getClientsPotencials() {
        List list = new ArrayList();
        list.add(getUserA());
        list.add(getUserB());
        list.add(getUserC());
        return list;
    }

    @Test
    void updateNewClients() {
        when(altamiraRepository.getDatosCliente(anyInt())).thenReturn(getClientA());
        when(clientRepository.findAll()).thenReturn(getClientsPotencials());
        when(clientRepository.saveAll(anyCollection())).thenReturn(getClientsPotencials());
        service.updateNewClients();
    }
}