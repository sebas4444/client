package ar.com.seba.bank.client.entities;

import lombok.Data;

import java.util.Date;

public @Data class Client {
    private Long id;
    private String name;
    private String lastName;
    private Date birthday;
    private Integer docNum;
    private Integer tipDoc;
}