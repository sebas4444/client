package ar.com.seba.bank.client.repositories;

import ar.com.seba.bank.client.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsersRepository extends JpaRepository <User, Long> {

}
