package ar.com.seba.bank.client.entities;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="AAMRTB_USUARIO")
public @Data class User {

    @Id
    @Column(name="USU_ID")
    private String id;
    @Column(name="USU_TIPO_DOC")
    private String docType;
    @Column(name="USU_NRO_DOC")
    private String docNum;

}
