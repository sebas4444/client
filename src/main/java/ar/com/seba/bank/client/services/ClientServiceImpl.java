package ar.com.seba.bank.client.services;

import ar.com.seba.bank.client.entities.Client;
import ar.com.seba.bank.client.entities.User;
import ar.com.seba.bank.client.repositories.AltamiraRepository;
import ar.com.seba.bank.client.repositories.ClientRepository;
import ar.com.seba.bank.client.repositories.UsersRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ClientServiceImpl implements ClientService {

    private ClientRepository clientRepository;
    private AltamiraRepository altamiraRepository;
    private UsersRepository usersRepository;

    public ClientServiceImpl(ClientRepository clientRepository, AltamiraRepository altamiraRepository, UsersRepository usersRepository) {
        this.clientRepository = clientRepository;
        this.altamiraRepository = altamiraRepository;
        this.usersRepository = usersRepository;
    }

    @Override
    public Client findByDocNum(Integer docNum) {
        return altamiraRepository.getDatosCliente(docNum);
    }

    @Override
    public List<User> getNewClientsPotential() {
        return usersRepository.findAll();
    }

    @Override
    public void updateNewClients() {
        List<User> docNumList = getNewClientsPotential();
        List<Client> clients = new ArrayList();
        docNumList.stream().forEach(user -> clients.add(findByDocNum(Integer.valueOf(user.getDocNum()))));
        clientRepository.saveAll(clients);
    }
}
