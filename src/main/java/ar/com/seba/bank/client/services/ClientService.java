package ar.com.seba.bank.client.services;

import ar.com.seba.bank.client.entities.Client;
import ar.com.seba.bank.client.entities.User;

import java.util.List;

public interface ClientService {

    /**
     * Obtiene Cliente a partir de un numero de Documento
     * @param docNum numero de documento
     * @return client
     */
    public Client findByDocNum(Integer docNum);
    public List<User> getNewClientsPotential ();
    public void updateNewClients();
}
