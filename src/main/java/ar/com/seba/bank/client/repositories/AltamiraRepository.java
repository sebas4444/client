package ar.com.seba.bank.client.repositories;

import ar.com.seba.bank.client.entities.Client;
import org.springframework.stereotype.Repository;

@Repository
public interface AltamiraRepository {

    public Client getDatosCliente(Integer numDoc);
}
