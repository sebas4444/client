package ar.com.seba.bank.client.controllers;

import ar.com.seba.bank.client.entities.User;
import ar.com.seba.bank.client.services.ClientService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ClientController {

    private ClientService clientService;

    public ClientController(ClientService clientService){
        this.clientService = clientService;
    }

    @GetMapping(value = "/users")
    public ResponseEntity<List<User>> users() {
        return ResponseEntity.ok(clientService.getNewClientsPotential());
    }

    }
